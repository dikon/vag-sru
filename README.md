# SRU-Daten zum Verlagsarchiv Gebauer-Schwetschke in DC/MODS

Bereitgestellt wurden die [hier](./data/) zu findenden Daten des [Findbuchs zum Verlagsarchiv Gebauer-Schwetschke](http://kalliope-verbund.info/de/findingaid?fa.id=DE-611-BF-37172&fa.enum=1&lastparam=true) über die [SRU-Schnittstelle](https://kalliope-verbund.info/de/support/sru.html) des Verbundskataloges [Kalliope](https://kalliope-verbund.info/). Hervorgegangen sind sie ursprünglich aus einem in den Jahren 2010 bis 2014 von der [DFG](https://gepris.dfg.de/gepris/projekt/165711239) geförderten [Erschließungs- und Digitalisierungsprojekt](http://www.gebauer-schwetschke.halle.de/gs/home/Projekt/), das kooperativ vom [Interdisziplinären Zentrum für die Erforschung der Europäischen Aufklärung](https://www.izea.uni-halle.de/forschung/abgeschlossene-projekte/gebauer-schwetschke.html) (IZEA) und dem [Stadtarchiv Halle (Saale)](https://www.halle.de/de/Kultur/Stadtgeschichte/Stadtarchiv/) durchgeführt wurde.

## Lizenz

„Für alle Metadaten, die im Online-Katalog des Kalliope-Verbunds öffentlich gemacht sind, gilt das Recht zur Bereitstellung unter der [CC-BY-SA Lizenz](./LICENSE) als erteilt.“ (Teilnahmevertrag am Kalliope-Verbund: [§ 3 Rechte an den Daten](https://kalliope-verbund.info/files/Kalliope-Teilnahmevertrag-20171101.pdf#page=5))

## Stand

Letzte Aktualisierung: 06.07.2020
