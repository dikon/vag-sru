---
title: "VAG: Beispieldaten"
output: html_document
---

```{r setup, include=F}
knitr::opts_chunk$set(echo = FALSE, message = FALSE,
    error = FALSE, warning = FALSE)
knitr::opts_knit$set(root.dir = "..")
```

```{r setup-python}
knitr::opts_chunk$set(engine.path = list(python = "env/bin/python"))
```

```{python py-setup}
import warnings
warnings.filterwarnings('ignore')
```

```{python py-librair}
from librair.schemas import xml
```

# Kalliope

_Dublin Core_

```{python example-dc}
gebauer_dc = xml.reader("data/records-archdesc-dc.xml")
```

```{python example-dc2, results="asis"}
gebauer_root = gebauer_dc.getroot()
for el in gebauer_root[1][1:]:
    el.getparent().remove(el)
print("```xml\n" + str(xml.Element(gebauer_root[1][0])) + "\n```" )
# num = gebauer_root[0]
# num.text = "1"
# xml.writer(gebauer_root, "example-archdesc-dc.xml", path="data")
```

_Metadata Object Description Schema_

```{python example-mods}
gebauer_mods = xml.reader("data/records-archdesc-mods.xml")
```

```{python example-mods2, results="asis"}
gebauer_root = gebauer_mods.getroot()
for el in gebauer_root[1][1:]:
    el.getparent().remove(el)
print("```xml\n" + str(xml.Element(gebauer_root[1][0])) + "\n```" )
# num = gebauer_root[0]
# num.text = "1"
# xml.writer(gebauer_root, "example-archdesc-mods.xml", path="data")
```
