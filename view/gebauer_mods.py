from librair.schemas import xml

tree = xml.reader("data/DE-611/sru/ead-archdesc--mods.xml")

mods = tree.find("records").getchildren()
len(mods)
# 46154

names = [m.findall(".//name", namespaces=m.nsmap) for m in mods
         if m.findall(".//name", namespaces=m.nsmap)]
len(names)
# 45624

corresp = {}

for e in names:
    for n in e:
        entype = n.attrib["type"]
        if entype not in corresp:
            corresp[entype] = {}
        if "authority" in n.attrib:      # and n.attrib["authority"] == GND:
            uri = n.attrib["valueURI"]
            if uri in corresp[entype]:
                corresp[entype][uri] += 1
            else:
                corresp[entype][uri] = 1
        else:
            if "no-id" in corresp[entype]:
                corresp[entype]["no-id"] += 1
            else:
                corresp[entype]["no-id"] = 1


len(corresp["personal"].keys())
# 1444
len(corresp["corporate"].keys())
# 235


corresp["personal"]["no-id"]
# 22626
corresp["corporate"]["no-id"]
# 826


corresp2 = {}

for e in names:
    for n in e:
        entype = n.attrib["type"]
        if entype not in corresp2:
            corresp2[entype] = {}
        if "authority" not in n.attrib:
            name = n.find("namePart", namespaces=n.nsmap).text
            if name in corresp2[entype]:
                corresp2[entype][name] += 1
            else:
                corresp2[entype][name] = 1

len(corresp2['personal'].keys())
# 4692
len(corresp2['corporate'].keys())
# 120
